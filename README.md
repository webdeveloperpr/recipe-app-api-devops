# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

## commands

**aws_vault**

- aws-vault exec luis.betancourt --duration=12h

**terraform**

- docker-compose -f deploy/docker-compose.yaml run --rm terraform fmt
- docker-compose -f deploy/docker-compose.yaml run --rm terraform validate
- docker-compose -f deploy/docker-compose.yaml run --rm terraform plan
- docker-compose -f deploy/docker-compose.yaml run --rm terraform apply
- docker-compose -f deploy/docker-compose.yaml run --rm terraform workspace list
- docker-compose -f deploy/docker-compose.yaml run --rm terraform workspace new dev