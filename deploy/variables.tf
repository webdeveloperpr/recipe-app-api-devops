variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "webdeveloperpr@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Instance"
}

variable "db_password" {
  description = "Username for the RDS Instance"
}